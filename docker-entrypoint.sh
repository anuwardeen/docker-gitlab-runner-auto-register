#!/bin/bash

# Important: this scripts depends on some predefined environment variables:
# - GITLAB_REGISTRATION_TOKEN (required): registration token for your project
# - RUNNER_TAG_LIST (optional): comma separated list of tags for the runner

unregister_runner() {
    curl --request DELETE "https://gitlab.com/api/v4/runners" --form "token=$1"
}

register_runner() {

    runner_identification="RUNNER_$(date +%s)"

    # Uses the environment variable "GITLAB_REGISTRATION_TOKEN" to register the runner

    result_json=$(
        curl --request POST "https://gitlab.com/api/v4/runners" \
            --form "token=$1" \
            --form "description=${runner_identification}" \
            --form "tag_list=$2"
    ) 

    # Read the authentication token

    auth_token=$(echo $result_json | jq -r '.token')

    # Create the config.toml based on our template

    cat /tmp/config_template.toml \
        | sed -e "s/\${RUNNER_NAME}/${runner_identification}/" \
        | sed -e "s/\${RUNNER_AUTH_TOKEN}/${auth_token}/" \
        > /etc/gitlab-runner/config.toml
}


register_runner ${GITLAB_REGISTRATION_TOKEN} ${RUNNER_TAG_LIST}

gitlab-runner run 

unregister_runner ${auth_token}

