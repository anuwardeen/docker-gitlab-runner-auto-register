# docker-gitlab-runner-auto-register

A docker image containing the Gitlab Runner that automatically registers itself as a Runner in your Gitlab project using a specified registration token.

Important: if the container is shutdown gracefully, the Runner will try to unregister itself in the Gitlab project before the container is stopped.


### Evolve config_template.toml if necessary

The config_template.toml presented in this repository is just an example. You may need to evolve it with runner-specific configurations for your project. Remember not to change the "name" and "token" variables, because they will be processed and replaced within the container startup.


### Evolve the Dockerfile as desired

Our Dockerfile does not install any specific development tools / frameworks (i.e: java, maven, graddle, etc).

As an example, if you will use the Runner as a shell executor, you may want to extend the Dockerfile with the installation of the necessary tools for your project.


### Discover the Gitlab registration token for your project

Go to Settings > CI/CD > Runners to obtain the token for your project.


### Build the docker image

```
docker build --rm -t docker-runner-auto-register .
```


### Run the container specifying the registration token

```
docker run -e GITLAB_REGISTRATION_TOKEN="my-registration-token" -it docker-runner-auto-register:latest
```

If you need to specify some tags for your Runner, you can also pass the variable "GITLAB_REGISTRATION_TOKEN" to your container. This variable's value should be a comma-separated list (i.e: -e RUNNER_TAG_LIST="other-custom,test-runner")


### For debugging

Connect to the container:
```
docker exec -it `docker ps -ql` /bin/bash

```

### Run on AWS Fargate

1. Build the docker image and push it to an ECR repository.

1. Create a Task Definition. During the configuration, set the container image as the one you pushed into ECR. Also, specify the container parameter "GITLAB_REGISTRATION_TOKEN" and set its value as the Runner registration token from your project. It is also relevant to properly configure the container stop timeout, since Fargate's default value may not be enough for the Runner to gracefully shutdown and deregister itself from Gitlab. Remember to properly configure the taskRole in case your task needs access to other AWS service (i.e: using Fargate custom driver).

1. Create a task based on the task definition or, if you need high availability, create an "ECS Service" that will be responsible for creating and managing a set of tasks based on your task definition.
